from isf.core import logger
import isf.hardware.ttltalker
import time
import string
from .config import prefixes
import re
import os


class CFE:
    baudrate = 115200
    com_port = '/dev/ttyACM0'
    uart_obj = None
    timeout = 0.1
    newline_bytes = '0d0a'
    turn_off_time = 5
    init_console_bytes = 'stop'.encode('charmap').hex()
    console_status = 0
    current_prefix = 'NOTEXIST'
    cmd_list_array = {}
    devices = {}

    def __init__(self, com_port="/dev/ttyUSB0", baudrate=115200, timeout=0.05,
                 newline_bytes="0d0a", turn_on_time=10,
                 init_console_bytes="73746f70", init_console=True,
                 cmd_prefix="00"):
        # copy from U-Boot module
        self.baudrate = baudrate
        self.com_port = com_port
        self.timeout = timeout
        self.newline_bytes = bytes.fromhex(newline_bytes).decode('charmap')
        self.turn_on_time = turn_on_time
        self.init_console_bytes = init_console_bytes
        self.newline_bytes_hex = newline_bytes
        self.console_status = not init_console
        if cmd_prefix == '00':
            self.cmd_prefix = prefixes
        else:
            self.cmd_prefix = [bytes.fromhex(cmd_prefix).decode('charmap')]
        self.create_connection()

    def create_connection(self):
        self.uart_obj = isf.hardware.ttltalker.TTLTalker(self.com_port,
                                                         self.baudrate,
                                                         self.timeout)
        if not self.uart_obj:
            logger.error("Can't connect to CFE console!")
            return -1

    def init_console(self):
        if not self.uart_obj:
            result = self.create_connection()
            if result == -1:
                return -1
        logger.info(
            "Please, turn off IoT device and wait for {} seconds!".format(
                self.turn_off_time))
        time.sleep(self.turn_off_time)
        start_time = time.time()
        print_once = 0
        ans = ''
        # 5 - turn on human time
        while time.time() - start_time < self.turn_on_time + 5:
            if not print_once:
                logger.info("Turn on IoT device.. NOW!")
                print_once = 1
            ans += self.uart_obj.send_bytes(self.init_console_bytes, True,
                                            10000, '')['ret_str'].decode(
                'charmap')
        # clearing console log
        while bytes.fromhex(self.init_console_bytes).decode('charmap') in ans:
            ans = self.clear_console()

        self.console_status = self.detect_prefix()

    def detect_prefix(self):
        logger.info("Checking for initiation of console")
        # self.clear_console()
        ans = self.uart_obj.send_bytes(self.newline_bytes_hex, True, 10000, '')
        if 'ret_str' in ans:
            logger.debug('Returned: {}'.format(ans['ret_str']))
        else:
            logger.debug('Nothing returned, may be errors!')
            return 0

        full_result = ans['ret_str'].decode('charmap') + self.clear_console()
        for prefix in prefixes:
            if prefix in full_result:
                self.current_prefix = prefix
                logger.debug('Current prefix: {}'.format(prefix))
                return 1
        logger.info(full_result)
        logger.error(
            "Can't find CFE CLI prefix. If it is not default, change it in config.py of CFE model.")
        return 0

    def clear_console(self):
        # only reading from console
        ans = self.uart_obj.read_output(length=10000)['ret_str']
        ans += self.uart_obj.read_output(length=10000)['ret_str']
        ans += self.uart_obj.read_output(length=10000)['ret_str']
        return ans.decode('charmap')

    def send_cmd(self, cmd="help"):
        if not self.console_status and self.console_status == 0:
            res = self.init_console()
            if res == -1:
                return -1
        elif self.current_prefix == 'NOTEXIST':
            self.detect_prefix()
        self.clear_console()
        logger.debug("Command: {}".format(cmd))
        logger.debug("HEX command: {}".format(cmd.encode("charmap").hex()))
        ans = self.uart_obj.send_bytes(cmd.encode("charmap").hex(), output=True,
                                       output_length=10000,
                                       newline_bytes=self.newline_bytes_hex)[
            'ret_str']
        full_result = ans.decode('charmap')
        logger.debug(full_result)
        # return
        i = 1
        while not full_result.endswith(self.current_prefix):
            # ans = \
            #    self.uart_obj.send_bytes('',
            #                             output=True,
            #                             output_length=10000,
            #                             newline_bytes='')['ret_str']
            ans = self.uart_obj.read_output(length=10000)['ret_str']
            rest_ans = ans.decode('charmap')
            full_result += rest_ans
            logger.debug(full_result)
            logger.debug(full_result.encode('charmap').hex())
            logger.debug(self.current_prefix)
            i += 1
            if i > 10:
                return
        logger.debug('RAW cmd answer:')
        logger.debug(full_result)
        while full_result.startswith(cmd + self.newline_bytes):
            full_result = full_result[len(cmd + self.newline_bytes):]
        while full_result.endswith(self.newline_bytes + self.current_prefix):
            full_result = full_result[
                          :-len(self.newline_bytes + self.current_prefix)]
        return {'result': full_result}

    def cmd_list(self):
        help_cmd = "help"
        r_command = "(.*[^ ]) {2,}(.*)"
        if self.cmd_list_array:
            return {'commands': self.cmd_list_array}
        ans = self.send_cmd(help_cmd)['result']

        arr = ans.split(self.newline_bytes)
        result = {}
        for line in arr:
            if bool(re.match(r_command, line)):
                res = re.match(r_command, line)
                result[res.groups()[0]] = res.groups()[1]
        self.cmd_list_array = result
        return {'commands': result}

    def device_list(self):
        if len(self.cmd_list_array) == 0:
            self.cmd_list()
        device_cmd = "show devices"
        r_device = "(.+[^ ])[ ]{2,}(.+)"
        if device_cmd not in self.cmd_list_array:
            logger.error('Command "{}" not found!'.format(device_cmd))
            return
        ans = self.send_cmd(device_cmd)['result'].strip(' \t\r\n')
        lines = ans.split(self.newline_bytes)
        devices = {}
        for line in lines:
            if bool(re.match(r_device,
                             line)) and \
                    'Device Name' not in line and not line.startswith('-'):
                g = re.match(r_device, line).groups()
                devices[g[0]] = g[1]
        self.devices = devices
        logger.warn("Some devices' names may be cutted, be careful!")
        return {'devices': devices}

    def memdevice_to_ram(self, device="nflash0.nvram", offset="0x40000",
                         size="0x1024"):
        if len(self.devices) == 0:
            self.device_list()
        logger.warn("Some devices' names may be cutted, be careful!")
        if device not in self.devices:
            logger.error('Device "{}" not found!'.format(device))
            logger.error('Availible devices: {}'.format(self.devices))
            return
        if 'size' not in self.devices[device]:
            logger.error('Device {} is not a type of memory'.format(device))
            return
        cmd_load = 'load'
        load_timeout = 5
        r_loaded_size = 'Loading:[ \\.]*([0-9]*) bytes read'
        if cmd_load not in self.cmd_list_array:
            logger.error('Command "{}" not found!'.format(cmd_load))
            return
        cmd = '{} -raw -max={} -addr={} {}:\r\n'.format(cmd_load, size,
                                                        offset, device)
        logger.info('Writing dump to memory! {} sec...'.format(load_timeout))

        tmp_timeout = self.uart_obj.serial_link.timeout
        self.uart_obj.serial_link.timeout = load_timeout
        ans = self.send_cmd(cmd)['result']
        self.uart_obj.serial_link.timeout = tmp_timeout
        loaded_size = 0
        for line in ans.split(self.newline_bytes):
            if bool(re.match(r_loaded_size, line)):
                loaded_size = int(re.match(r_loaded_size, line).groups()[0])
        if loaded_size == 0:
            logger.error('Zero size of file -> read/write error')
        return {'size': loaded_size}

    def dram_info(self, show_all=True):
        cmd_dram = 'show memory'  # {}'.format('-a' if show_all else '')
        r_mem = '([0-9a-fA-F]+)\\-([0-9a-fA-F]+) \\(([0-9a-fA-F]+)\\) (.+)'
        if len(self.cmd_list_array) == 0:
            self.cmd_list()
        if cmd_dram not in self.cmd_list_array:
            logger.error('Command "{}" not found!'.format(cmd_dram))
            return
        cmd = cmd_dram
        if show_all:
            cmd += ' -a'
        ans = self.send_cmd(cmd)['result']
        lines = ans.split(self.newline_bytes)
        result = []
        for line in lines:
            if bool(re.match(r_mem, line)):
                g = re.match(r_mem, line).groups()
                new_memory = dict()
                new_memory['start'] = int(g[0], 16)
                new_memory['end'] = int(g[1], 16)
                new_memory['size'] = int(g[2], 16)
                new_memory['description'] = g[3]
                result.append(new_memory)
        return {'memory': result}

    def read_ram(self, addr=262144, length=100, file="1.txt",
                 dump_type="serial"):
        if dump_type == 'serial':
            cmd_dump = 'd'
            r_bytes = "([0-9a-fA-F]{8}): ([0-9a-fA-F]{8}) ([0-9a-fA-F]{8}) ([0-9a-fA-F]{8}) ([0-9a-fA-F]{8})[ ]+(.{16})"
            if len(self.cmd_list_array) == 0:
                self.cmd_list()
            if cmd_dump not in self.cmd_list_array:
                logger.error('Command "{}" not found!'.format(cmd_dump))
                return

            res = b''
            real_read_addr = addr - (addr % 0x10)
            while len(res) - (addr % 0x10) < length:
                if real_read_addr % 0x1000 == 0:
                    logger.info('Reading from {}'.format(hex(real_read_addr)))
                cmd = cmd_dump + ' ' + hex(real_read_addr)
                ans = self.send_cmd(cmd)['result']
                lines = ans.split(self.newline_bytes)
                tmp_str = b''
                for line in lines:
                    if bool(re.match(r_bytes, line)):
                        g = re.match(r_bytes, line).groups()
                        tmp_str += b''.join(
                            [bytes.fromhex(x)[::-1] for x in g[1:5]])

                if len(tmp_str) != 0x100:
                    logger.error(
                        'Error at {}, repeating..'.format(real_read_addr))
                else:
                    res += tmp_str
                    real_read_addr += 0x100
            res = res[addr % 0x10:]
            res = res[:length]
            f = open(file, 'wb')
            f.write(res)
            f.close()
            logger.info(
                'File {} with size {} was created!'.format(file, len(res)))
            return {'size': len(res)}
        elif dump_type == 'microsd':
            pass  # TODO
        elif dump_type == 'tftp':
            pass  # TODO

    def dump_device(self, device="nflash0.nvram", dump_type="serial",
                    file="1.txt"):
        if len(self.devices) == 0:
            self.device_list()
        if device not in self.devices:
            logger.error('Device "{}" not found!'.format(device))
            logger.error('Availible devices: {}'.format(self.devices))
            return
        if 'size' not in self.devices[device]:
            logger.error('Device {} is not a type of memory'.format(device))
            return

        if dump_type == 'serial':
            # get size of flash
            size = 0
            tmp_address = 0x40000
            s_size = self.devices[device].split(' ')[-1]
            if 'KB' in s_size:
                size = int(s_size.strip(' \t\r\n').replace('KB', '')) * 1024
            elif 'MB' in s_size:
                size = int(
                    s_size.strip(' \t\r\n').replace('MB', '')) * 1024 * 1024
            # TODO: add other sizes

            # size=100

            logger.info(
                'Loading {} into address {}.'.format(device, tmp_address))
            loaded_size = \
            self.memdevice_to_ram(device, hex(tmp_address), hex(size))[
                'size']
            # loaded_size=100
            return self.read_ram(tmp_address, loaded_size, file, dump_type)
        elif dump_type == 'microsd':
            pass  # TODO
        elif dump_type == 'tftp':
            pass  # TODO

    def get_env(self):
        cmd_env = 'printenv'
        r_variable = '([^ ]+) {2,}([^ ].*)'
        if len(self.cmd_list_array) == 0:
            self.cmd_list()
        if cmd_env not in self.cmd_list_array:
            logger.error('Command "{}" not found!'.format(cmd_env))
            return
        ans = self.send_cmd(cmd_env)['result']
        lines = ans.split(self.newline_bytes)
        result = {}
        for line in lines:
            if bool(re.match(r_variable, line)) and not line.startswith(
                    'Variable Name'):
                g = re.match(r_variable, line).groups()
                result[g[0]] = g[1]
        return {'variables': result}

    def get_nvram(self):
        cmd_nvram = 'nvram'
        r_variable = '([^= ]+)=(.*)'
        if len(self.cmd_list_array) == 0:
            self.cmd_list()
        if cmd_nvram not in self.cmd_list_array:
            logger.error('Command "{}" not found!'.format(cmd_nvram))
            return
        cmd = cmd_nvram + ' show'
        ans = self.send_cmd(cmd)['result']
        lines = ans.split(self.newline_bytes)
        result = {}
        for line in lines:
            if bool(re.match(r_variable, line)):
                g = re.match(r_variable, line).groups()
                result[g[0]] = g[1]
        return result

    def set_nvram(self, name="test", value="test", commit=False, check=True):
        cmd_nvram = 'nvram'
        if len(self.cmd_list_array) == 0:
            self.cmd_list()
        if cmd_nvram not in self.cmd_list_array:
            logger.error('Command "{}" not found!'.format(cmd_nvram))
            return
        cmd = "{} set '{}'='{}'".format(
            cmd_nvram,
            name.replace('\\', '\\\\').replace('"', '\\"'),
            value.replace('\\', '\\\\').replace('"', '\\"'))
        self.send_cmd(cmd)
        if commit:
            cmd = '{} commit'
            self.send_cmd(cmd)
        if check:
            logger.info('Checking variable..')
            var = self.get_nvram()
            if name not in var:
                return 0
            return var[name] == value
        return 1
